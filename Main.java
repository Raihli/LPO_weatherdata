package com.again.java;

import com.again.java.utilities.Calc;
import com.again.java.utilities.Date;
import com.again.java.utilities.Parsing;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {
        String[] date = Date.askingDate();
        final String LINK =
                "https://raw.githubusercontent.com/lyndadotcom/LPO_weatherdata/master/Environmental_Data_Deep_Moor_" + date[0] + ".txt";
        URL url = new URL(LINK);
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String[] infoLine = new String[8];
        ArrayList<String[]> totalInfo = new ArrayList<>();
        String inputLine;

        while ((inputLine = in.readLine()) != null) {
            // Substring the date
            String dateTime = inputLine.substring(0, 10);

            // Compare the date from the file and our date
            if (dateTime.equals(date[0] + "_" + date[1] + "_" + date[2])) {
                Parsing.parsingElements(inputLine, infoLine);
                totalInfo.add(infoLine);
            }
        }

        in.close();

        if (totalInfo.size() == 0) {
            System.err.println("No information for this date, please retry");
            Date.askingDate();
        }
        Calc.calculator(totalInfo);
    }
}
