package com.again.java.utilities;

import java.util.ArrayList;
import java.util.Arrays;

public class Calc {
    public static void calculator(ArrayList<String[]> totalInfo) {
        double[] windSpeedArray = new double[totalInfo.size()];
        double[] airTemperatureArray = new double[totalInfo.size()];
        double[] barometricPressureArray = new double[totalInfo.size()];
        int i = -1;

        while (++i < totalInfo.size()) {
            String[] infoLine = totalInfo.get(i);

            // Stock Wind Speed
            windSpeedArray[i] = Double.parseDouble(infoLine[7]);

            // Stock Air Temperature
            airTemperatureArray[i] = Double.parseDouble(infoLine[1]);

            // Stock Barometric Pressure
            barometricPressureArray[i] = Double.parseDouble(infoLine[2]);
        }

        System.out.println("Wind Speed Mean: " + mean(windSpeedArray));
        System.out.println("Wind Speed Median: " + median(windSpeedArray));
        System.out.println("Air Temperature Mean: " + mean(airTemperatureArray));
        System.out.println("Air Temperature Median: " + median(airTemperatureArray));
        System.out.println("Barometric Pressure Mean: " + mean(barometricPressureArray));
        System.out.println("Barometric Pressure Median: " + median(barometricPressureArray));

    }

    private static double mean(double[] array) {
        double result = 0;

        for (double element : array)
            result += element;
        result /= array.length;

        return (result);
    }

    private static double median(double[] array) {
        Arrays.sort(array);
        if (array.length % 2 == 0)
            return ((array[array.length / 2] + array[array.length / 2 + 1]) / 2);
        else
            return (array[array.length / 2]);
    }
}
