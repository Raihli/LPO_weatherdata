package com.again.java.utilities;

import java.util.Scanner;

public class Date {
    public static String[] askingDate() {
        Scanner sc = new Scanner(System.in);
        String[] date = new String[3];

        System.out.print("Year: ");
        date[0] = sc.nextLine();
        System.out.print("Month: ");
        date[1] = sc.nextLine();
        System.out.print("Day: ");
        date[2] = sc.nextLine();

        if (!checkDate(date))
            askingDate();

        return date;
    }

    private static boolean checkDate(String[] date) {
        int year = Integer.parseInt(date[0]);
        int month = Integer.parseInt(date[1]);
        int day = Integer.parseInt(date[2]);

        if (year < 2013 || year > 2015) {
            System.err.println("Year must be between 2013 and 2015 included");
            return (false);
        }

        if (month < 1 || month > 12) {
            System.err.println("Month must be between 1 and 12 included");
            return (false);
        }

        if (day < 1 || day > 31) {
            System.err.println("Day must be between 1 and 31 included");
            return (false);
        }

        return (true);
    }
}
